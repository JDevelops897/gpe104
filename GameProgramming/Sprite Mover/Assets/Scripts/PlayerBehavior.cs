﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour {
	
	/*
	I would once again like to reiterate that I have no idea what I'm doing
	the code in this project is hacky and terrible and I apologize.
	-Jordan
	*/

	//move speed
	public float speed = 0.1f;

	//establish variables for properties
	private Transform tf;
	private Animator an;
	// Use this for initialization
	void Start () {
		//set tf to the current Transform
		tf = GetComponent<Transform>();
		//zero the tf
		tf.position = Vector3.zero;

		//set an to the current Animator
		an = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		//create temporary movement vector
		Vector3 movement = tf.position;

		//inputs
		if (!Input.GetKey (KeyCode.LeftShift)) {
			//if shift is not held, move by speed every frame
			if (Input.GetKey ("right")) {
				movement.x += speed;
			}
			if (Input.GetKey ("left")) {
				movement.x -= speed;
			}
			if (Input.GetKey ("up")) {
				movement.y += speed;
			}
			if (Input.GetKey ("down")) {
				movement.y -= speed;
			}
		} else {
			//if shift is held, move one unit per key press
			if (Input.GetKeyDown ("right")) {
				movement.x += 1;
			}
			if (Input.GetKeyDown ("left")) {
				movement.x -= 1;
			}
			if (Input.GetKeyDown ("up")) {
				movement.y += 1;
			}
			if (Input.GetKeyDown ("down")) {
				movement.y -= 1;
			}
		}

		//if space is pressed, center the object
		if (Input.GetKeyDown (KeyCode.Space)) {
			movement = Vector3.zero;
		}

		//if the player is moving, animate. If not, disable animation
		if (tf.position == movement) {
			an.enabled = false;
		} else {
			an.enabled = true;
		}

		//apply position changes
		tf.position = movement;
	}
}
