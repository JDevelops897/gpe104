﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour {

	//create variables for the player object and the player script
	private GameObject p;
	private PlayerBehavior pb;

	// Use this for initialization
	void Start () {
		//find the things they reference
		p = GameObject.Find ("Knight");
		pb = p.GetComponent<PlayerBehavior> ();
	}
	
	// Update is called once per frame
	void Update () {
		//toggle PlayerBehavior when P is pressed
		if (Input.GetKeyDown(KeyCode.P)) {
			pb.enabled = !pb.enabled;
			p.GetComponent<Animator> ().enabled = false;
		}
		//toggle the Player object being active when Q is pressed
		if (Input.GetKeyDown(KeyCode.Q)) {
			p.SetActive (!p.activeSelf);
		}
		//End the program when Escape is pressed
		if (Input.GetKeyDown(KeyCode.Escape)) {
			//Application.Quit();
			UnityEditor.EditorApplication.isPlaying = false;
		}
	}
}
