﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour {

	/*
	IN CASE YOU READ ANY OF THIS:
	I am completely new to Unity. I have no idea what I'm doing.
	Numbers are arbitrary and make no sense.
	My naming conventions are inconsistnt.
	I couldn't figure out animation systems.

	sorry about all that
	-jordan
	*/

	//Set variables
	//gravity
	private float gravity = 0.0f;
	//move speed
	private float speed = 0.1f;

	//establish variables for properties
	private Transform tf;
	private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		//set tf to the current Transform
		tf = GetComponent<Transform>();
		//zero the tf
		tf.position = Vector3.zero;

		sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		//update gravity 
		if (gravity < .5f && tf.position.y != -4.5f) {
			//increase fall speed if below is empty
			gravity += .01f;
		} else if (gravity > .5f && tf.position.y != -4.5f) {
			//cap fall speed at .5
			gravity = .5f;
		}
		//create temporary movement vector
		Vector3 movement = tf.position;
		//apply gravity
		movement.y -= gravity;
		//if the new position is bottom of the map
		if (movement.y < -4.5f) {
			//keep the y in bounds
			movement.y = -4.5f;
			//reset gravity
			gravity = 0f;
		}

		//inputs
		if (Input.GetKey("right")) {
			sr.flipX = false;
			movement.x += speed;
		}
		if (Input.GetKey("left")) {
			sr.flipX = true;
			movement.x -= speed;
		}
		//jump
		if (Input.GetKeyDown("up") && movement.y == -4.5f) {
			gravity = -.25f;
		}

		//apply position changes
		tf.position = movement;
	}
}