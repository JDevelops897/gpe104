﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehavior : MonoBehaviour {

	/* 

	I really don't know what I'm doing and feel terrible about myself

	*/

	private Transform tf;

	private float Speed = 0.00f;
	private float Thrust = 0.00f;

	private int FacingDirection = 0;

	private Vector3 movementV;
	private Vector3 thrustV;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () {

		movementV = Vector3.zero;

		if (Input.GetKey("right")) {
			FacingDirection += 5;
			tf.Rotate(Vector3.back * 5);
		}
		if (Input.GetKey("left")) {
			FacingDirection -= 5;
			tf.Rotate(Vector3.forward * 5);
		}

		if (Input.GetKey ("up") && Speed < 0.01) {
			Speed += 0.001f;
			Thrust = 0.001f;
		} else if (Input.GetKey ("up") && Speed > 0.01) {
			Speed = 0.01f;
			Thrust = 0.001f;
		} else if (Input.GetKey ("up") == false && Speed > 0) {
			Speed -= 0.001f;
			Thrust = 0.0f;
		} else {
			Speed = 0.0f;
			Thrust = 0.00f;
		}

		thrustV.x += (Thrust * Mathf.Cos((FacingDirection * Mathf.PI)/180));
		thrustV.y += (Thrust * -Mathf.Sin((FacingDirection * Mathf.PI)/180));

		movementV += (thrustV);

		tf.position += (movementV);
	}

	void OnCollisionEnter2D(Collision2D other) {
		Destroy(other.gameObject);
	}
	void OnTriggerExit2D() {
		Destroy(this.gameObject);
	}
}