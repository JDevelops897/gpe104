﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehavior : MonoBehaviour {

	private Transform tf;

	private float Speed;
	private int Direction;

	private Vector3 movementV;

	// Use this for initialization
	void Start () {
		Speed = Random.Range(0.01f, 0.05f);
		Direction = Random.Range(0, 360);

		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		movementV = tf.position;

		movementV.x += (Speed * Mathf.Cos((Direction * Mathf.PI)/180));
		movementV.y += (Speed * -Mathf.Sin((Direction * Mathf.PI)/180));

		tf.position = (movementV);
	}

	void OnTriggerExit2D() {
		Destroy(this.gameObject);
	}
}
