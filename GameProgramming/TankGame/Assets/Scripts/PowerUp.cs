﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

	public enum powerUpType {
		speed,
		mg,
		shield,
		heal
	}
		
	public GameObject spawner;
	public powerUpType type;
	private AudioSource audioData;

	public AudioClip powerUpSound;

	private float yPos;
	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		//randomise the rotation/bobbing
		yPos = Random.Range (0, 180);
		//set color depending on type of powerup
		switch (type) {
		case powerUpType.speed:
			GetComponent<Renderer> ().material.color = Color.green;
			break;
		case powerUpType.mg:
			GetComponent<Renderer> ().material.color = Color.red;
			break;
		case powerUpType.shield:
			GetComponent<Renderer> ().material.color = Color.blue;
			break;
		case powerUpType.heal:
			GetComponent<Renderer> ().material.color = Color.white;
			break;
		}
		audioData = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		//constantly rotate and bob up and down
		tf.position = new Vector3 (tf.position.x, 1 + Mathf.Sin(yPos), tf.position.z);
		tf.eulerAngles = new Vector3 (tf.eulerAngles.x, Time.frameCount+yPos, tf.eulerAngles.z);
		yPos += 0.1f;
	}

	void OnTriggerEnter(Collider c) {
		//if a tank has triggered this
		if (c.gameObject.tag == "Tank") {
			//activate the powerup of this type
			switch (type) {
			case powerUpType.speed:
				c.gameObject.GetComponent<TankData> ().moveSpeedCurrent = c.gameObject.GetComponent<TankData> ().moveSpeed*2;
				c.gameObject.GetComponent<TankData> ().powerUpCooldownSpeed = 60 * 5;
				break;
			case powerUpType.mg:
				c.gameObject.GetComponent<TankData> ().shotCoolDownCurrent = c.gameObject.GetComponent<TankData> ().shotCoolDown/5;
				c.gameObject.GetComponent<TankData> ().powerUpCooldownMachinegun = 60 * 5;
				break;
			case powerUpType.shield:
				c.gameObject.GetComponent<TankData> ().shield = true;
				c.gameObject.GetComponent<TankData> ().powerUpCooldownShield = 60 * 5;
				break;
			case powerUpType.heal:
				c.gameObject.GetComponent<TankData> ().health += 50;
				break;
			}
			//set the spawner timer
			spawner.GetComponent<PowerUpSpawner> ().spawnTimer = GameManager.instance.powerUpTime;
			spawner.GetComponent<PowerUpSpawner> ().powerUpSpawned = false;
			//play sound
			if (GameManager.instance.SFXEnabled) 
				GameManager.instance.audioData.PlayOneShot (powerUpSound, (GameManager.instance.SFXVolume / 100.0f));
			Destroy (this.gameObject);
		}
	}
}
