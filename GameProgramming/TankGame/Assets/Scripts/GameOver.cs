﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	void Start () {
		//set the high score UI on the gameover screen
		transform.Find ("P1Score").gameObject.GetComponent<Text> ().text = "Player 1 Score: " + GameManager.instance.p1Score.ToString ();
		if (GameManager.instance.multiplayer) {
			transform.Find ("P2Score").gameObject.SetActive (true);
			transform.Find ("P2Score").gameObject.GetComponent<Text> ().text = "Player 2 Score: " + GameManager.instance.p2Score.ToString ();
		} else {
			transform.Find ("P2Score").gameObject.SetActive (false);
		}
		transform.Find ("HighScore").gameObject.GetComponent<Text> ().text = "High Score: " + GameManager.instance.highScore.ToString ();
	}

	//funcition goes from gameover scene back to normal scene. resets all variables to reset the game
	public void RestartGame() {
		SceneManager.LoadScene ("TankScene", LoadSceneMode.Single);
		GameManager.instance.enemies.Clear ();
		GameManager.instance.playerSpawnPoints.Clear ();
		GameManager.instance.enemySpawnPoints.Clear ();
		GameManager.instance.powerUpSpawnPoints.Clear ();
		GameManager.instance.waypoints.Clear ();
		GameManager.instance.waypoints = new List<List<Transform>> ();
	}

	public void EndGame() {
		GameManager.instance.QuitGame ();
	}
}
