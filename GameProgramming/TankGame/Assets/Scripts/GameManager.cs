﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	public List<List<Transform>> waypoints = new List<List<Transform>> ();
	public List<Transform> playerSpawnPoints;
	public List<Transform> enemySpawnPoints;
	public List<Transform> powerUpSpawnPoints;
	public List<TankData> enemies;
	public List<PlayerController> players;

	public PlayerController player;

	public GameObject mainCamera;
	public GameObject map;
	public MapGenerator mapGen;
	public GameObject UI;

	public GameObject enemyTank;
	public GameObject enemyAI;
	public GameObject playerTank;
	public GameObject playerObject;
	public GameObject playerCam;

	public int powerUpTime;

	public int seed;

	public int highScore = 0;
	public int p1Score = 0;
	public int p2Score = 0;

	private int count = 0;

	public bool gameStarted = false;
	public bool options = false;

	public float MapRows = 2;
	public float MapColumns = 2;
	public bool MapOfTheDay = false;

	public int SFXVolume = 100;
	public int MusicVolume = 100;
	public bool SFXEnabled = true;
	public bool MusicEnabled = true;

	public bool multiplayer = false;

	public AudioSource audioData;

	//on awake make sure the manager cannot be destroyed on scene loads
	void Awake() {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad(gameObject);
		} else {
			Destroy(gameObject);
		}
	}

	void Start() {
		//load the data 
		Load ();
		audioData = GetComponent<AudioSource> ();
		//set each UI element to what it should be
		UI.GetComponent<MainMenu> ().UpdateUI ();
	}

	// Update is called once per frame
	void Update () {
		//if it's in the tank scene (not game over)
		if (SceneManager.GetActiveScene ().name == "TankScene") {
			//make sure each reference still exists
			if (map == null) 
				GameManager.instance.map = GameObject.Find ("Map");
			if (mapGen == null)
				GameManager.instance.mapGen = GameObject.Find ("MapGenerator").GetComponent<MapGenerator> ();
			if (UI == null)
				GameManager.instance.UI = GameObject.Find ("Canvas");
			if (mainCamera == null)
				GameManager.instance.mainCamera = GameObject.Find ("Main Camera");
			
			//check if the players exist so you can set the camera
			if (players.Count == 0) {
				mainCamera.GetComponent<Camera>().enabled = true;
			} else {
				mainCamera.GetComponent<Camera>().enabled = false;
				//if there's at least one player
				if (players.Count > 0) {
					//set each camera's size depending on how many players
					for (int i = 0; i < players.Count; i++) {
						try {
							players [i].data.GetComponent<CameraFollow> ().cam.GetComponent<Camera> ().rect = new Rect (
								new Vector2(0.0f, (1.0f / (i+1.0f)) - (1.0f / players.Count)), 
								new Vector2(1.0f, (1.0f / players.Count)));
						} catch (System.Exception e) {
							Debug.Log ("Player Tank missing");
						}
					}
				}
			}
		}
		//if the game is running
		if (gameStarted) {
			count++;
			if (count % 240 == 0) {
				//SpawnEnemyTank (new Vector3 (0, 2, 0), AiController.AIType.Patrol);
			}

			//if the number of enemies is less than 1/3 of the rooms, spawn another one
			if (enemies.Count < enemySpawnPoints.Count/3) {
				SpawnEnemyTank (Random.Range (0, enemySpawnPoints.Count), (AiController.AIType)Random.Range (0, 4));
			}

			//if the players have all died
			if (player == null && players.Count == 0) {
				//go to game over screen
				SceneManager.LoadScene ("GameOver", LoadSceneMode.Single);
				gameStarted = false;
			}
		}
	}

	//spawn player function
	void SpawnPlayer(int room) {
		//Instantiate player and tank pawn
		GameObject t = Instantiate(playerTank, new Vector3(playerSpawnPoints[room].position.x, 1, playerSpawnPoints[room].position.z), new Quaternion(0,0,0,0));
		GameObject p = Instantiate (playerObject, new Vector3(playerSpawnPoints[room].position.x, 1, playerSpawnPoints[room].position.z), new Quaternion (0, 0, 0, 0));
		//Instantiate camera
		t.GetComponent<CameraFollow> ().cam = Instantiate(playerCam);
		t.GetComponent<TankData> ().team = 0;

		p.GetComponent<PlayerController> ().data = t.GetComponent<TankData> ();
		p.GetComponent<PlayerController> ().cam = t.GetComponent<CameraFollow> ().cam;
		player = p.GetComponent<PlayerController> ();
		//add player to list of players
		players.Add (p.GetComponent<PlayerController>());

		t.GetComponent<TankData> ().controller = p;

		//if this is player 2, change his controls to WASD
		if (players.Count == 2) {
			p.GetComponent<PlayerController> ().forwardKey = KeyCode.W;
			p.GetComponent<PlayerController> ().backwardKey = KeyCode.S;
			p.GetComponent<PlayerController> ().turnRightKey = KeyCode.D;
			p.GetComponent<PlayerController> ().turnLeftKey = KeyCode.A;
			p.GetComponent<PlayerController> ().shootKey = KeyCode.LeftControl;
		}
	}

	//Function to spawn an enemy
	void SpawnEnemyTank(int room, AiController.AIType type) {
		//spawn the tank at the given position
		GameObject t = Instantiate (enemyTank,  new Vector3(enemySpawnPoints[room].position.x, 1, enemySpawnPoints[room].position.z), new Quaternion(0,0,0,0));
		t.GetComponent<TankData> ().team = 1;
		//color the tank depending on the AI Type
		switch (type) {
		case AiController.AIType.Patrol:
			t.GetComponent<Renderer> ().material.color = Color.Lerp(Color.red, Color.blue, .1f);
			break;
		case AiController.AIType.Aggressive:
			t.GetComponent<Renderer> ().material.color = Color.Lerp(Color.red, Color.black, .1f);
			break;
		case AiController.AIType.Passive:
			t.GetComponent<Renderer> ().material.color = Color.Lerp(Color.red, Color.blue, .6f);
			break;
		case AiController.AIType.Defensive:
			t.GetComponent<Renderer> ().material.color = Color.Lerp(Color.red, Color.black, .75f);
			break;
		}

		//Create the controller for the tank
		GameObject a = Instantiate (enemyAI, new Vector3(enemySpawnPoints[room].position.x, 1, enemySpawnPoints[room].position.z), new Quaternion(0,0,0,0));
		//set the controller variables
		a.GetComponent<AiController> ().data = t.GetComponent<TankData> ();
		a.GetComponent<AiController> ().waypoints = waypoints[room];
		a.GetComponent<AiController> ().aiType = type;
		//add the enemy to the list of enemies
		enemies.Add (t.GetComponent<TankData> ());

		t.GetComponent<TankData> ().controller = a;

	}

	//Start a new game
	public void StartGame() {
		//check the menu for a seed. if the seed field is not empty
		if (UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("InputField").gameObject.GetComponent<InputField> ().text != "") {
			//set the seed to the input field and gen the map
			seed = int.Parse (UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("InputField").gameObject.GetComponent<InputField> ().text);
			mapGen.GenerateMap (Mathf.RoundToInt (MapRows), Mathf.RoundToInt (MapColumns), seed);
		} else {
			//generate a map randomly or mapoftheday depending on setting
			if (MapOfTheDay)
				mapGen.GenerateMap (Mathf.RoundToInt (MapRows), Mathf.RoundToInt (MapColumns), System.DateTime.Now.Day);
			else
				mapGen.GenerateMap (Mathf.RoundToInt (MapRows), Mathf.RoundToInt (MapColumns), Time.frameCount);
		}
		//spawn a player
		SpawnPlayer (Random.Range(0, playerSpawnPoints.Count));
		//if it's 2 player, spawn another
		if (multiplayer) 
			SpawnPlayer (Random.Range(0, playerSpawnPoints.Count));
		//destroy the main menu
		Destroy (UI);
		//start the game
		gameStarted = true;
		audioData.Play ();
		map.GetComponent<AudioSource> ().Play ();
	}

	//function for the menu to set the number of rows in the map
	public void SetNumRows(float r) {
		MapRows = r;
		UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumRowsSlider").gameObject.transform.Find ("Text").gameObject.GetComponent<Text> ().text = "Number of Rows: " + r.ToString ();
		audioData.Play ();
	}

	//function for the menu to set the number of columns in the map
	public void SetNumCols(float c) {
		MapColumns = c;
		UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumColsSlider").gameObject.transform.Find ("Text").gameObject.GetComponent<Text> ().text = "Number of Columns: " + c.ToString ();
		audioData.Play ();
	}

	//function for the menu to set if Map of the day is enabled
	public void SetMapOfTheDay(bool motd) {
		MapOfTheDay = motd;
		audioData.Play ();
	}

	//function for the menu, toggles if you are in the options menu or not
	public void ToggleOptions() {
		options = !options;
		if (options) {
			UI.transform.Find ("OptionsMenu").gameObject.SetActive (true);
			UI.transform.Find ("MainMenu").gameObject.SetActive (false);
		} else {
			UI.transform.Find ("OptionsMenu").gameObject.SetActive (false);
			UI.transform.Find ("MainMenu").gameObject.SetActive (true);
		}
		audioData.Play ();
	}

	//quit game function, saves data then quits
	public void QuitGame() {
		Save ();
		#if UNITY_EDITOR
		if (UnityEditor.EditorApplication.isPlaying) {
			UnityEditor.EditorApplication.isPlaying = false;
		}
		#endif
		Application.Quit ();
	}

	//set SFX volume 
	public void SetSFXVolume(float v) {
		SFXVolume = Mathf.RoundToInt(v);
		UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("SFXVolumeSlider").gameObject.transform.Find ("Text").gameObject.GetComponent<Text> ().text = "SFX Volume: " + v.ToString ();
		audioData.volume = (SFXVolume/100.0f);
		if (!audioData.isPlaying)
			audioData.Play ();
	}

	//set Music volume
	public void SetMusicVolume(float v) {
		MusicVolume = Mathf.RoundToInt(v);
		UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MusicVolumeSlider").gameObject.transform.Find ("Text").gameObject.GetComponent<Text> ().text = "Music Volume: " + v.ToString ();
		map.GetComponent<AudioSource> ().volume = (MusicVolume/100.0f);
		if (!audioData.isPlaying)
			audioData.Play ();
	}

	//Toggles if SFX are enabled or not
	public void ToggleSFX(bool s) {
		SFXEnabled = !SFXEnabled;
		audioData.mute = !SFXEnabled;
		audioData.Play ();
	}

	//toggles if Music is enabled or not
	public void ToggleMusic(bool s) {
		MusicEnabled = !MusicEnabled;
		map.GetComponent<AudioSource> ().mute = !MusicEnabled;
		audioData.Play ();
	}

	//toggles if it's 1 or 2 players
	public void ToggleMultiplayer(bool m) {
		audioData.Play ();
		multiplayer = !multiplayer;
	}

	//load all data from playerprefs
	private void Load() {
		if (PlayerPrefs.HasKey ("highScore")) {
			highScore = PlayerPrefs.GetInt ("highScore");
		}
		if (PlayerPrefs.HasKey ("SFXEnabled")) {
			SFXEnabled = (PlayerPrefs.GetInt ("SFXEnabled") != 0);
		}
		if (PlayerPrefs.HasKey ("MusicEnabled")) {
			MusicEnabled = (PlayerPrefs.GetInt ("MusicEnabled") != 0);
		}
		if (PlayerPrefs.HasKey ("SFXVolume")) {
			SFXVolume = PlayerPrefs.GetInt ("SFXVolume");
		}
		if (PlayerPrefs.HasKey ("MusicVolume")) {
			MusicVolume = PlayerPrefs.GetInt ("MusicVolume");
		}
		if (PlayerPrefs.HasKey ("MapOfTheDay")) {
			MapOfTheDay = (PlayerPrefs.GetInt ("MapOfTheDay") != 0);
		}
		if (PlayerPrefs.HasKey ("MapRows")) {
			MapRows = PlayerPrefs.GetInt ("MapRows");
		}
		if (PlayerPrefs.HasKey ("MapColumns")) {
			MapColumns = PlayerPrefs.GetInt ("MapColumns");
		}
		if (PlayerPrefs.HasKey ("multiplayer")) {
			multiplayer = (PlayerPrefs.GetInt ("multiplayer") != 0);
		}
	}

	//save all data to playerprefs
	public void Save() {
		PlayerPrefs.SetInt ("highScore", highScore);
		PlayerPrefs.SetInt ("SFXEnabled", SFXEnabled ? 1 : 0);
		PlayerPrefs.SetInt ("MusicEnabled", MusicEnabled ? 1 : 0);
		PlayerPrefs.SetInt ("SFXVolume", SFXVolume);
		PlayerPrefs.SetInt ("MusicVolume", MusicVolume);
		PlayerPrefs.SetInt ("MapOfTheDay", (MapOfTheDay ? 1 : 0));
		PlayerPrefs.SetInt ("MapRows", Mathf.RoundToInt(MapRows));
		PlayerPrefs.SetInt ("MapColumns", Mathf.RoundToInt(MapColumns));
		PlayerPrefs.SetInt ("multiplayer", multiplayer ? 1 : 0);
	}

}