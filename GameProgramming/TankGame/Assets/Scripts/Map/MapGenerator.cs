﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	public List<GameObject> tiles;

	public void GenerateMap(int numRows, int numCols, int seed) {
		Random.InitState (seed);
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				int randomRoom = Random.Range (0, tiles.Count);
				GameObject r = Instantiate (tiles [randomRoom], new Vector3 (i*25, 0 , j*25), new Quaternion (0, 0, 0, 0));
				Room rData = r.GetComponent<Room> ();
				if (i == 0)	Destroy(rData.doorEast);
				if (i == numRows-1)	Destroy(rData.doorWest);
				if (i > 0 && i < numRows - 1) {
					Destroy(rData.doorEast);
					Destroy(rData.doorWest);
				}
				if (j == 0)	Destroy(rData.doorNorth);
				if (j == numCols-1)	Destroy(rData.doorSouth);
				if (j > 0 && j < numCols - 1) {
					Destroy(rData.doorNorth);
					Destroy(rData.doorSouth);
				}
				GameManager.instance.enemySpawnPoints.Add (rData.enemySpawn);
				GameManager.instance.playerSpawnPoints.Add (rData.playerSpawn);
				GameManager.instance.powerUpSpawnPoints.Add (rData.powerUpSpawn);
				GameManager.instance.waypoints.Add (rData.waypoints);
				r.GetComponent<Transform> ().parent = GameManager.instance.map.GetComponent<Transform>();
			}
		}
	}
}