﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour {

	private Transform tf;

	public TankData data;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		data = GetComponent<TankData> ();
	}
	//shoot function
	public void Shoot(GameObject shell) {
		//create a shell at the position of the cannon, oriented same as tank
		GameObject s = Instantiate (shell, new Vector3(tf.position.x, tf.position.y+.05f, tf.position.z)-tf.forward, tf.rotation);
		//give the speed to the shell
		s.GetComponent<ShellBehavior> ().Speed = data.shellSpeed;
		//assign the parent tank
		s.GetComponent<ShellBehavior> ().tank = this.gameObject;
		//set the decay time on the shell
		s.GetComponent<ShellBehavior> ().delay = data.shellTime;
		//play shot shound if SFX are not muted
		if (GameManager.instance.SFXEnabled)
			data.audioData.PlayOneShot(data.shotSound, GameManager.instance.SFXVolume / 100.0f);
	}
}
