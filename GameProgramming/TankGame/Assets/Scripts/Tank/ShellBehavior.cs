﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellBehavior : MonoBehaviour {

	public float Speed;
	public float delay;
	public GameObject tank;

	private AudioSource audioData;
	public AudioClip bulletHit;
	private Transform tf;
	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		audioData = GetComponent<AudioSource> ();
		//set the self destruct timer
		Destroy (this.gameObject, delay);
	}

	// Update is called once per frame
	void Update () {
		//move forward constantly
		tf.position += tf.forward * Speed;
	}

	//if colliding with something
	void OnTriggerEnter(Collider c) {
		//if the object it's colliding with is not the parent tank
		try {
			if (c.gameObject != tank) {
				//check if it is another tank
				if (c.gameObject.tag == "Tank") {
					if (!c.gameObject.GetComponent<TankData> ().shield && c.gameObject.GetComponent<TankData>().team != tank.GetComponent<TankData>().team) {
						//if it is, do damage
						c.GetComponent<TankData> ().health -= tank.GetComponent<TankData> ().shellDamage;
						//if the damage kills the tank
						if (c.GetComponent<TankData> ().health <= 0) {
							//add score
							tank.GetComponent<TankData> ().score += c.GetComponent<TankData> ().killReward;
							try {
								tank.GetComponent<TankData> ().controller.GetComponent<PlayerController>().score += c.GetComponent<TankData> ().killReward;
							} catch (System.Exception e) {
								//
							}
							try {
								tank.GetComponent<TankData> ().controller.GetComponent<AiController>().score += c.GetComponent<TankData> ().killReward;
							} catch (System.Exception e) {
								//
							}
							GameManager.instance.enemies.Remove (c.GetComponent<TankData> ());
							if (GameManager.instance.SFXEnabled)
								GameManager.instance.audioData.PlayOneShot (c.GetComponent<TankData>().deathSound, (GameManager.instance.SFXVolume / 100.0f));
							//destroy enemy tank
							Destroy (c.gameObject);
						}
					}
				}
				//after colliding, destroy shell
				if (c.gameObject.tag != "Shell") {
					if (GameManager.instance.SFXEnabled)
						GameManager.instance.audioData.PlayOneShot (bulletHit, (GameManager.instance.SFXVolume / 100.0f));
					Destroy (this.gameObject);
				}
			}
		} catch (System.Exception e) {
			Debug.Log(e);
		}
	} 
}
