﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover : MonoBehaviour {

	[HideInInspector] public Transform tf;

	public TankData data;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		data = GetComponent<TankData> ();
	}

	//moveforward/moveback use movespeed*deltatime to move the tf forward or back
	public void MoveForward(float moveSpeed) {
		tf.position += (new Vector3(tf.forward.x, 0, tf.forward.z))*moveSpeed*Time.deltaTime;
		data.noise = 15;
	}

	public void MoveBack(float moveSpeed) {
		tf.position -= (new Vector3(tf.forward.x, 0, tf.forward.z))*moveSpeed*Time.deltaTime;
		data.noise = 15;
	}

	//rotateright/rotateleft use rotatespeed*deltatime to rotate the tf left or right
	public void RotateRight(float rotateSpeed) {
		tf.Rotate (tf.up*rotateSpeed*Time.deltaTime);
	}

	public void RotateLeft(float rotateSpeed) {
		tf.Rotate (-tf.up*rotateSpeed*Time.deltaTime);
	}

	public void RotateTowards(Vector3 targetDirection, float rotateSpeed) {
		//find rotation that looks down the vector targetRotation
		Quaternion targetRotation = Quaternion.LookRotation (targetDirection);
		//rotate "less than turnspeed" degrees towards targetRotation
		tf.rotation = Quaternion.RotateTowards (tf.rotation, targetRotation, rotateSpeed*Time.deltaTime);
	}

}