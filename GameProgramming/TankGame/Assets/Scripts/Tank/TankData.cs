﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {

	public AudioClip shotSound;
	public AudioClip deathSound;

	public float moveSpeed;
	public float moveSpeedCurrent;
	public float rotateSpeed;
	public float shotCoolDown;
	public float shotCoolDownCurrent;
	public float shellSpeed;
	public float health;
	public float maxHealth;
	public float shellDamage;
	public float shellTime;
	public int killReward;
	public int score = 0;
	public bool bounce = false;
	public bool machinegun = false;
	public bool shield = false;

	public int powerUpCooldownSpeed = 0;
	public int powerUpCooldownMachinegun = 0;
	public int powerUpCooldownShield = 0;

	public int noise = 0;

	public int team;

	private Color oColor;
	public AudioSource audioData;
	public GameObject shell;

	[HideInInspector] public TankMover mover;
	[HideInInspector] public TankShooter shooter;
	public GameObject controller;

	// Use this for initialization
	void Start () {
		mover = GetComponent<TankMover> ();
		shooter = GetComponent<TankShooter> ();
		oColor = GetComponent<Renderer> ().material.color;
		audioData = GetComponent<AudioSource> ();
	}

	void Update() {
		//decrease noise every step until no noise left
		if (noise > 0) {
			noise--;
		}

		//cooldown the Speed powerup
		if (powerUpCooldownSpeed > 0) {
			powerUpCooldownSpeed--;
		} else {
			moveSpeedCurrent = moveSpeed;
		}

		//cooldown the shield powerup
		if (powerUpCooldownShield > 0) {
			powerUpCooldownShield--;
			GetComponent<Renderer> ().material.color = Color.Lerp (oColor, Color.blue, .9f);
		} else {
			shield = false;
			GetComponent<Renderer> ().material.color = oColor;
		}

		//cooldown the machinegun powerup
		if (powerUpCooldownMachinegun > 0) {
			powerUpCooldownMachinegun--;
		} else {
			machinegun = false;
			shotCoolDownCurrent = shotCoolDown;
		}
	}
}