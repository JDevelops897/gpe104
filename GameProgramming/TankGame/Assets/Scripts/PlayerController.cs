﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public TankData data;
	private float timer = 0;

	public KeyCode forwardKey = KeyCode.UpArrow;
	public KeyCode backwardKey = KeyCode.DownArrow;
	public KeyCode turnRightKey = KeyCode.RightArrow;
	public KeyCode turnLeftKey = KeyCode.LeftArrow;
	public KeyCode shootKey = KeyCode.Space;

	private GameObject UI;

	public int score = 0;
	public int lives = 3;

	public GameObject cam;

	// Use this for initialization
	void Start () {
		UI = data.GetComponent<CameraFollow>().cam.transform.Find("Canvas").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		//count down timer if it's over 0
		if (timer > 0) {
			timer -= 1/60f;
		}

		if (data != null) {

			//use mover functions
			if (Input.GetKey (forwardKey)) {
				data.mover.MoveForward (data.moveSpeedCurrent);
			}
			if (Input.GetKey (backwardKey)) {
				data.mover.MoveBack (data.moveSpeedCurrent);
			}
			if (Input.GetKey (turnRightKey)) {
				data.mover.RotateRight (data.rotateSpeed);
			}
			if (Input.GetKey (turnLeftKey)) {
				data.mover.RotateLeft (data.rotateSpeed);
			}

			//if the cooldown is done and player pressed space, fire shell and reset cooldown
			if (Input.GetKeyDown (shootKey) && timer <= 0) {
				data.shooter.Shoot (data.shell);
				timer = data.shotCoolDownCurrent;
			}

		} else {
			//player has died
			//check if extra lives remaining
			if (lives <= 0) { 
				//none remaining check which player died and set their high score
				if (GameManager.instance.players.Count == 2) {
					GameManager.instance.p2Score = score;
					GameManager.instance.player = GameManager.instance.players[0];
				} else {
					GameManager.instance.p1Score = score;
					GameManager.instance.player = null;
				}
				if (score > GameManager.instance.highScore) {
					GameManager.instance.highScore = score;
					GameManager.instance.Save ();
				}
				//remove the player from the list and destroy them
				GameManager.instance.players.Remove (this.gameObject.GetComponent<PlayerController> ());
				Destroy (cam);
				Destroy (this.gameObject);
			} else {
				//player has extra lives
				//spawn a new one at a random position
				int room = Random.Range (0, GameManager.instance.playerSpawnPoints.Count);
				GameObject t = Instantiate(GameManager.instance.playerTank, new Vector3(GameManager.instance.playerSpawnPoints[room].position.x, 1, GameManager.instance.playerSpawnPoints[room].position.z), new Quaternion(0,0,0,0));
				data = t.GetComponent<TankData> ();
				t.GetComponent<CameraFollow> ().cam = cam;
				lives--;
			}
		}

		//Update each UI Element to show the current health, score, high score, and lives remaining
		UI.transform.Find ("Health").gameObject.GetComponent<Text> ().text = "Health: " + data.health.ToString ();
		UI.transform.Find ("Score").gameObject.GetComponent<Text> ().text = "Score: " + score.ToString ();
		UI.transform.Find ("HighScore").gameObject.GetComponent<Text> ().text = "High Score: " + GameManager.instance.highScore.ToString ();
		UI.transform.Find ("Lives").gameObject.GetComponent<Text> ().text = "Extra Lives: " + lives.ToString ();

	}
}
