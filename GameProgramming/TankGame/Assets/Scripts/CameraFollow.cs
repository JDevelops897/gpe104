﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	private Transform tf;

	public GameObject cam;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		//constantly set the camera x and z to the player x and z
		cam.transform.position = new Vector3 (tf.position.x, cam.transform.position.y, tf.position.z);
	}
}
