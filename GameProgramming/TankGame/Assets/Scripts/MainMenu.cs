﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//when the main menu starts, make sure that every UI element has a listener and make sure they all show the correct value
		//EXTREMELY UGLY AND BAD SCRIPT
		//I WAS HACKING THIS UI TOGETHER BECAUSE IT WAS MY FIRST TIME USING UNITY UIS
		//VERY SORRY
		GameManager.instance.UI = this.gameObject;
		GameManager.instance.UI.transform.Find ("MainMenu").gameObject.transform.Find ("Begin Button").gameObject.GetComponent<Button> ().onClick.AddListener (GameManager.instance.StartGame);
		GameManager.instance.UI.transform.Find ("MainMenu").gameObject.transform.Find ("Options Button").gameObject.GetComponent<Button> ().onClick.AddListener (GameManager.instance.ToggleOptions);
		GameManager.instance.UI.transform.Find ("MainMenu").gameObject.transform.Find ("Quit Button").gameObject.GetComponent<Button> ().onClick.AddListener (GameManager.instance.QuitGame);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("SFXVolumeSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.SFXVolume;
		GameManager.instance.SetSFXVolume (GameManager.instance.SFXVolume);
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("SFXVolumeSlider").gameObject.GetComponent<Slider> ().onValueChanged.AddListener (GameManager.instance.SetSFXVolume);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MusicVolumeSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.MusicVolume;
		GameManager.instance.SetMusicVolume (GameManager.instance.MusicVolume);
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MusicVolumeSlider").gameObject.GetComponent<Slider> ().onValueChanged.AddListener (GameManager.instance.SetMusicVolume);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MapOfTheDay").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.MapOfTheDay;
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MapOfTheDay").gameObject.GetComponent<Toggle> ().onValueChanged.AddListener (GameManager.instance.SetMapOfTheDay);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumRowsSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.MapRows;
		GameManager.instance.SetNumRows (GameManager.instance.MapRows);
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumRowsSlider").gameObject.GetComponent<Slider> ().onValueChanged.AddListener (GameManager.instance.SetNumRows);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumColsSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.MapColumns;
		GameManager.instance.SetNumCols (GameManager.instance.MapColumns);
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumColsSlider").gameObject.GetComponent<Slider> ().onValueChanged.AddListener (GameManager.instance.SetNumCols);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("ToggleSFX").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.SFXEnabled;
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("ToggleSFX").gameObject.GetComponent<Toggle> ().onValueChanged.AddListener (GameManager.instance.ToggleSFX);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("ToggleMusic").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.MusicEnabled;
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("ToggleMusic").gameObject.GetComponent<Toggle> ().onValueChanged.AddListener (GameManager.instance.ToggleMusic);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("Toggle2Player").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.multiplayer;
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("Toggle2Player").gameObject.GetComponent<Toggle> ().onValueChanged.AddListener (GameManager.instance.ToggleMultiplayer);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("Back Button").gameObject.GetComponent<Button> ().onClick.AddListener (GameManager.instance.ToggleOptions);
	}

	public void UpdateUI() {
		//Set the value of each UI element so they look appropriate
		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("SFXVolumeSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.SFXVolume;
		GameManager.instance.SetSFXVolume (GameManager.instance.SFXVolume);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MusicVolumeSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.MusicVolume;
		GameManager.instance.SetMusicVolume (GameManager.instance.MusicVolume);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("MapOfTheDay").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.MapOfTheDay;

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumRowsSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.MapRows;
		GameManager.instance.SetNumRows (GameManager.instance.MapRows);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("NumColsSlider").gameObject.GetComponent<Slider> ().value = GameManager.instance.MapColumns;
		GameManager.instance.SetNumCols (GameManager.instance.MapColumns);

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("ToggleSFX").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.SFXEnabled;

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("ToggleMusic").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.MusicEnabled;

		GameManager.instance.UI.transform.Find ("OptionsMenu").gameObject.transform.Find ("Toggle2Player").gameObject.GetComponent<Toggle> ().isOn = GameManager.instance.multiplayer;
		}

}
