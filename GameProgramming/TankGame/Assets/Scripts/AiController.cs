﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : MonoBehaviour {
	
	public TankData data;

	public List<Transform> waypoints;
	public int currentWaypoint;
	public float waypointCutoff;

	public enum AIType {Aggressive, Patrol, Passive, Defensive};
	public AIType aiType;

	public enum AIStates {Idle, Patrol, Chase, Flee};
	public AIStates currentState;
	private float timeInCurrentState = 0;
	public float chaseDistance;
	public float fleeHealthPercent;
	public float fleeTime;
	public float chaseTime;

	public bool isPatrolling;
	public bool isAdvancingWaypoints = true;
	public float avoidanceDistance;

	public enum PatrolType { Stop, Loop, PingPong, End };
	public PatrolType patrolType;

	public enum AvoidState { Normal, TurnToAvoid, MoveToAvoid };
	public AvoidState avoidState = AvoidState.Normal;
	public float avoidDuration = 1.0f;
	public float avoidStateTime;

	public float fieldOfView;
	public float visionDistance;
	public float shootDistance;

	private float shootTimer = 0;
	public int score = 0;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	public virtual void Update () {

		//count down timer if it's over 0
		if (shootTimer > 0) {
			shootTimer -= 1/60f;
		}

		if (data == null) {
			Destroy (this.gameObject);
		}

		//Count the time in the current state
		timeInCurrentState += Time.deltaTime;

		switch (aiType) {

		////////////////////////////////////////////////
		case AIType.Aggressive:
			//Agressive AI Type
			shootDistance = 10f;
			chaseDistance = 12.5f;
			visionDistance = 20f;

			patrolType = PatrolType.PingPong;

			switch (currentState) {
			//Agressive AI still patrols like normal but is less likely to flee
			case AIStates.Patrol:
				Patrol ();
				//if the player is seen or heard, go into chase state unless health is too low
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent/2) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				}
				break;
			case AIStates.Chase:
				Chase ();
				if (timeInCurrentState > chaseTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			case AIStates.Idle:
				Idle ();
				//if the player is seen or heard, return to the flee/chase cycle. otherwise patrol
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent/2) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				} else {
					ChangeState (AIStates.Patrol);
				}
				break;
			case AIStates.Flee:
				Flee ();
				if (timeInCurrentState > fleeTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			}
			break;
		////////////////////////////////////////////////
		////////////////////////////////////////////////
		case AIType.Patrol:
			//Patrol AI Type
			shootDistance = 7.5f;
			chaseDistance = 7.5f;
			visionDistance = 20f;

			patrolType = PatrolType.Loop;

			switch (currentState) {
			//Default state is to patrol the points
			case AIStates.Patrol:
				Patrol ();
				//if the player is seen or heard, go into chase state unless health is too low
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				}
				break;
			case AIStates.Chase:
				Chase ();
				if (timeInCurrentState > chaseTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			case AIStates.Idle:
				Idle ();
				//if the player is seen or heard, return to the flee/chase cycle. otherwise patrol
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				} else {
					ChangeState (AIStates.Patrol);
				}
				break;
			case AIStates.Flee:
				Flee ();
				if (timeInCurrentState > fleeTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			}
			break;
		////////////////////////////////////////////////
		////////////////////////////////////////////////
		case AIType.Passive:
			//Passive AI Type
			shootDistance = 10f;
			chaseDistance = 2.5f;
			visionDistance = 10f;

			patrolType = PatrolType.End;

			switch (currentState) {
			//Passive will not patrol. does not easily chase and shoots slower from a distance
			case AIStates.Patrol:
				//if the player is seen or heard, go into chase state unless health is too low
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent*2) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				}
				break;
			case AIStates.Chase:
				Chase ();
				if (timeInCurrentState > chaseTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			case AIStates.Idle:
				Idle ();
				//if the player is seen or heard, return to the flee/chase cycle. otherwise patrol
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent*2) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				} else {
					ChangeState (AIStates.Patrol);
				}
				break;
			case AIStates.Flee:
				Flee ();
				if (timeInCurrentState > fleeTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			}
			break;
		////////////////////////////////////////////////
		////////////////////////////////////////////////
		case AIType.Defensive:
			//Defensive AI Type
			shootDistance = 15f;
			chaseDistance = 10f;
			visionDistance = 25f;

			patrolType = PatrolType.End;

			switch (currentState) {
			//Defensive AI Patrols but keeps distance from the player and does not aggressively chase like others
			case AIStates.Patrol:
				Patrol ();
				//if the player is seen or heard, go into chase state unless health is too low
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				}
				break;
			case AIStates.Chase:
				Chase ();
				if (timeInCurrentState > chaseTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			case AIStates.Idle:
				Idle ();
				//if the player is seen or heard, return to the flee/chase cycle. otherwise patrol
				if (CanSeePlayer() || CanHearPlayer()) {
					if ((data.health / data.maxHealth * 100) < fleeHealthPercent) {
						ChangeState (AIStates.Flee);
					} else {
						ChangeState (AIStates.Chase);
					}
				} else {
					ChangeState (AIStates.Patrol);
				}
				break;
			case AIStates.Flee:
				Flee ();
				if (timeInCurrentState > fleeTime) {
					ChangeState (AIStates.Idle);
				}
				break;
			}
			break;
		////////////////////////////////////////////////
		}

	}

	protected void Idle() {
		//NOTHING
	}

	protected void Chase() {
		try {
			if (GameManager.instance.player != null) {
				if (Vector3.Distance (data.mover.tf.position, GameManager.instance.player.data.mover.tf.position) > shootDistance) {
					MoveTowards (GameManager.instance.player.data.mover.tf.position);
				} else {
					Vector3 targetPosition = new Vector3 (GameManager.instance.player.data.mover.tf.position.x, data.mover.tf.position.y, GameManager.instance.player.data.mover.tf.position.z);

					// Find direction to "current" waypoint
					Vector3 dirToWaypoint = targetPosition - data.mover.tf.position;

					// Turn towards the "current" waypoint
					data.mover.RotateTowards (dirToWaypoint, data.rotateSpeed);
				}
				if (shootTimer <= 0 && CanSeePlayer ()) {
					data.shooter.Shoot (data.shell);
					shootTimer = data.shotCoolDownCurrent*2;
				}
			}
		}  catch (System.Exception e) {
			Debug.Log("Player Tank missing");
		}
	}

	protected void Flee() {
		try {
			
				if (GameManager.instance.player != null) {
					//Get the vector to the player
					Vector3 vectorToPlayer = GameManager.instance.player.data.mover.tf.position - data.mover.tf.position;
					//flip it to be away from player
					vectorToPlayer = -1 * vectorToPlayer;
					//normalize and multiply by speed to be a short distance away
					vectorToPlayer.Normalize ();
					//move towards that point
					MoveTowards (data.mover.tf.position + (vectorToPlayer * 3));
					Debug.DrawLine (data.mover.tf.position, data.mover.tf.position + (vectorToPlayer * 3), Color.blue);
				}
			
		} catch (System.Exception e) {
			Debug.Log("Player Tank missing");
		}
	}

	protected void MoveTowards(Vector3 target) {
		if (avoidState == AvoidState.Normal) {
			if (CanMoveForward()) {
				// Make a new target
				Vector3 targetPosition = new Vector3(target.x, data.mover.tf.position.y, target.z);

				// Find direction to "current" waypoint
				Vector3 dirToWaypoint = targetPosition - data.mover.tf.position;

				// Turn towards the "current" waypoint
				data.mover.RotateTowards(dirToWaypoint, data.rotateSpeed);

				// move forward
				data.mover.MoveForward(data.moveSpeedCurrent);
			} else {
				avoidState = AvoidState.TurnToAvoid;
			}
		} else if (avoidState == AvoidState.TurnToAvoid) {
			data.mover.RotateRight(data.rotateSpeed);

			if (CanMoveForward()) {
				avoidState = AvoidState.MoveToAvoid;
				avoidStateTime = Time.time;
			}
		} else if (avoidState == AvoidState.MoveToAvoid) {
			//If I can Move Forward, do so
			if (CanMoveForward()) {
				data.mover.MoveForward (data.moveSpeedCurrent);
			} else {
				// If I can't move forward 
				// Go back to my turn to avoid state
				avoidState = AvoidState.TurnToAvoid;
			}

			// If I have moved forward for X seconds
			if (Time.time >= avoidStateTime + avoidDuration) {
				// Go back to the normal state
				avoidState = AvoidState.Normal;
			}
		}
	}

	protected bool CanMoveForward() {
		Debug.DrawLine (new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+1.5f, data.mover.tf.position.z), new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+1.5f, data.mover.tf.position.z) + (data.mover.tf.forward * avoidanceDistance), Color.red);
		Debug.DrawLine (new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+0.5f, data.mover.tf.position.z)+(data.mover.tf.right*1.25f), new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+0.5f, data.mover.tf.position.z)+(data.mover.tf.right*1.25f) + (data.mover.tf.forward * avoidanceDistance), Color.red);
		Debug.DrawLine (new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+0.5f, data.mover.tf.position.z)-(data.mover.tf.right*1.25f), new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+0.5f, data.mover.tf.position.z)-(data.mover.tf.right*1.25f) + (data.mover.tf.forward * avoidanceDistance), Color.red);

		//Check if you can move forward. If you can't return false (Checking the full width of the tank)
		if (Physics.Raycast (new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+1.5f, data.mover.tf.position.z), data.mover.tf.forward, avoidanceDistance) || 
			Physics.Raycast	(new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+0.5f, data.mover.tf.position.z)+(data.mover.tf.right*1.25f), data.mover.tf.forward, avoidanceDistance) ||
			Physics.Raycast	(new Vector3(data.mover.tf.position.x, data.mover.tf.position.y+0.5f, data.mover.tf.position.z)-(data.mover.tf.right*1.25f), data.mover.tf.forward, avoidanceDistance)) {
			return false;
		}

		//if it didn't return false, there is nothing in the way
		return true;
	}

	protected void Patrol() {
		//draw a line to the waypoint
		Debug.DrawLine(data.mover.tf.position+data.mover.tf.up, new Vector3(waypoints[currentWaypoint].position.x, data.mover.tf.position.y+1, waypoints[currentWaypoint].position.z), Color.green);

		//make sure waypoint is within range
		currentWaypoint = Mathf.Clamp(currentWaypoint, 0, waypoints.Count-1);

		//move toward waypoint
		MoveTowards(waypoints[currentWaypoint].position);

		//if close enough (x and z only) switch to next waypoint
		if (Vector3.Distance(data.mover.tf.position, new Vector3(waypoints[currentWaypoint].position.x, data.mover.tf.position.y, waypoints[currentWaypoint].position.z)) <= waypointCutoff) {
			//Advance the waypoint
			if (patrolType == PatrolType.PingPong) {
				//Ping Pong moves front to back then back to front
				//just have to check which direction it's going
				if (isAdvancingWaypoints) {
					currentWaypoint++;
				} else {
					currentWaypoint--;
				}
			} else {
				currentWaypoint++;
			}

			//Do what happens at the end of the list of waypoints
			if (currentWaypoint >= waypoints.Count) {
				if (patrolType == PatrolType.Loop) {
					//loop
					currentWaypoint = 0;
				} else if (patrolType == PatrolType.Stop) {
					//Stop patrolling
					isPatrolling = false;
				} else if (patrolType == PatrolType.End) {
					currentWaypoint--;
					//Do Nothing
				} else if (patrolType == PatrolType.PingPong) {
					//Reverse
					isAdvancingWaypoints = false;
					currentWaypoint = currentWaypoint - 2;
				}
			} else if (currentWaypoint < 0) {
				//Reverse directions to go forward again
				isAdvancingWaypoints = true;
				currentWaypoint = 1;
			}
		}
	}

	public void ChangeState(AIStates newState) {
		//set the state to a new one and reset the timer
		currentState = newState;
		timeInCurrentState = 0;
	}

	public bool CanSeePlayer() {
		try {
			if (GameManager.instance.player != null) {
				//If the player is within chase distance, automatically return true
				if (Vector3.Distance (data.mover.tf.position, GameManager.instance.player.data.mover.tf.position) <= chaseDistance) {
					return true;
				}
				//If the player is within sight distance
				if (Vector3.Distance (data.mover.tf.position, GameManager.instance.player.data.mover.tf.position) <= visionDistance) {
					//check if player is within FOV
					if (Vector3.Angle (GameManager.instance.player.data.mover.tf.position - data.mover.tf.position, data.mover.tf.forward) < fieldOfView / 2) {
						//check if there is a direct line of sight on player
						if (!Physics.Raycast (new Vector3 (data.mover.tf.position.x, data.mover.tf.position.y + 1.5f, data.mover.tf.position.z), GameManager.instance.player.data.mover.tf.position - data.mover.tf.position, Vector3.Distance (data.mover.tf.position, GameManager.instance.player.data.mover.tf.position) - 5)) {
							return true;
						}
					}
				}
			}
		} catch (System.Exception e) {
			Debug.Log("Player Tank missing");
		}
		return false;
	}

	public bool CanHearPlayer() {
		try {
			if (GameManager.instance.player != null) {
				//if the distance to the player is less than the noise value that the player is currently making
				if (Vector3.Distance (data.mover.tf.position, GameManager.instance.player.data.mover.tf.position) <= GameManager.instance.player.data.noise) {
					return true;
				}
			}
		} catch (System.Exception e) {
			Debug.Log("Player Tank missing");
		}
		return false;
	}

}