﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour {

	public int spawnTimer = 1;
	public bool powerUpSpawned = false;

	public List<GameObject> powerUps;

	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (spawnTimer > 0 && !powerUpSpawned) {
			spawnTimer--;
		} else if (spawnTimer <= 0 && !powerUpSpawned) {
			SpawnNewPowerup ();
		}
	}

	void SpawnNewPowerup () {
		GameObject p = Instantiate (powerUps [Random.Range (0, powerUps.Count)], tf);
		p.GetComponent<PowerUp> ().spawner = gameObject;
		p.GetComponent<PowerUp> ().type = (PowerUp.powerUpType)Random.Range(0, 4);
		powerUpSpawned = true;
	}
}
