﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehavior : MonoBehaviour {

	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("right")) {
			tf.Rotate(Vector3.back * 5);
		}
		if (Input.GetKey("left")) {
			tf.Rotate(Vector3.forward * 5);
		}

		if (Input.GetKey ("up")) {
			tf.position += tf.up*.1f;
		}
	}
}
