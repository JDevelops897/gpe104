﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_harpoonBehavior : MonoBehaviour {

	private Transform tf;
	private Rigidbody2D rb;
	private AudioSource a;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		rb = GetComponent<Rigidbody2D> ();
		a = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

		//rotate the harpoon to the facing direction
		Vector2 dir = rb.velocity;
		float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
		tf.rotation = Quaternion.RotateTowards (tf.rotation, q, 360 * Time.deltaTime);

		//if the harpoon is too low, destroy it
		if (tf.position.y < -10) {
			Destroy (gameObject);
		}

	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.tag == "minnow") {
			Destroy (other.gameObject);
			GameObject c = GameObject.Find ("obj_controller");
			c.GetComponent<scr_control> ().kills += 1;
			c.GetComponent<scr_control> ().score += 100;
			a.Play ();
		}
		if (other.gameObject.tag == "flyingfish") {
			Destroy (other.gameObject);
			GameObject c = GameObject.Find ("obj_controller");
			c.GetComponent<scr_control> ().kills += 1;
			c.GetComponent<scr_control> ().score += 300;
			a.Play ();
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "swordfish") {
			Destroy (other.gameObject);
			GameObject c = GameObject.Find ("obj_controller");
			c.GetComponent<scr_control> ().kills += 1;
			c.GetComponent<scr_control> ().score += 750;
			a.Play ();
		}
	}

}
