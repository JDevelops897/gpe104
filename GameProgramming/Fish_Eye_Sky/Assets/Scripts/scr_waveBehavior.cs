﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_waveBehavior : MonoBehaviour {

	private float pointOfRotationX;
	private float pointOfRotationY;

	public float rotationSpeed;
	public float rotationRadius;
	private float rotationDegrees;

	private Transform tf;

	// Use this for initialization
	void Start () {
		//get the transform and set the point of rotation to the initial point
		tf = GetComponent<Transform> ();
		pointOfRotationX = tf.position.x;
		pointOfRotationY = tf.position.y;

		//get a random radius between 6 and 8, then set the degree to something random
		rotationRadius = Random.Range(6f, 8f);
		rotationDegrees = Random.Range(0f, 360f);

		//set the wave to -4 Y
		tf.position = new Vector3 (tf.position.x, -4, tf.position.z);
	}

	// Update is called once per frame
	void Update () {
		//increment the degrees of rotation around the point
		rotationDegrees += rotationSpeed / rotationRadius;

		float x, y;
		//set x and y to a position around the point at the distance of radius at the current angle
		x = pointOfRotationX + rotationRadius * (Mathf.Cos (rotationDegrees)*Mathf.Deg2Rad);
		y = pointOfRotationY - rotationRadius * (Mathf.Sin (rotationDegrees)*Mathf.Deg2Rad);

		//move the point slightly left
		pointOfRotationX -= .005f;

		//if the point is far enough off screen, teleport it to the right
		if (pointOfRotationX < -8) {
			pointOfRotationX = 8;
		}

		//apply position changes
		tf.position = new Vector3 (x, y, tf.position.z);
	}
}
