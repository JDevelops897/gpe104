﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_fishBehavior : MonoBehaviour {

	private Transform tf;
	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		rb = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {

		//rotate the harpoon to the facing direction
		Vector2 dir = rb.velocity;
		float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
		tf.rotation = Quaternion.RotateTowards (tf.rotation, q, 360 * Time.deltaTime);

		//if the harpoon is too low, destroy it
		if (tf.position.y < -10) {
			Destroy (gameObject);
		}

	}
}
