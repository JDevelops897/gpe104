﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_shipBehavior : MonoBehaviour {

	public GameObject shipTop;
	public GameObject cannon;
	public GameObject harpoon;

	private Transform tf;

	public float bobSpeed;
	private float bobRadius = .1f;
	private float bobDegrees = 0;

	private int shotCooldown = 0;

	private float rotationDegrees = -1.55f;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
		//set the ship position so the cannon is centered and the ship is bobbing in the water
		tf.position = new Vector3 (-1.5f, -5.6f, -1f);
		//rotate cannon 90 degrees so it is upright
		cannon.transform.Rotate (Vector3.forward*90);
	}
	
	// Update is called once per frame
	void Update () {
		//increment the bobbing
		bobDegrees += bobSpeed;

		//set y to a position based on the bobbing
		float y;
		y = -5.8f - bobRadius * Mathf.Sin (bobDegrees);

		//apply position
		tf.position = new Vector3 (tf.position.x, y, tf.position.z);

		//increment ship tilt
		rotationDegrees += .01f;

		//rotate the ship (both parts, top and bottom)
		tf.transform.Rotate (Vector3.forward * Mathf.Sin (rotationDegrees)*.03f);
		shipTop.transform.Rotate (Vector3.forward * Mathf.Sin (rotationDegrees)*.03f);

		//set the top of the ship to the same position as the bottom
		shipTop.transform.position = new Vector3 (tf.position.x, tf.position.y-.01f, shipTop.transform.position.z);

		//find the vector for the position of the cannon at any given rotation of the ship
		Vector3 p = new Vector3 (tf.position.x + 1.5f, tf.position.y + .2f, cannon.transform.position.z);
		Vector3 v = Quaternion.Euler(0, 0, tf.eulerAngles.z)*(p-tf.position)+tf.position;

		//apply the vector and set the cannon position
		cannon.transform.position = new Vector3(v.x, v.y, cannon.transform.position.z);

		//if A or D is pressed, rotate the cannon
		if (shotCooldown < 20) {
			if (Input.GetKey ("a") && cannon.transform.eulerAngles.z < 165) {
				cannon.transform.Rotate (Vector3.forward);
			}
			if (Input.GetKey ("d") && cannon.transform.eulerAngles.z > 27) {
				cannon.transform.Rotate (-Vector3.forward);
			}
		}
		//if space is pressed, fire a harpoon
		if (Input.GetKeyDown (KeyCode.Space) && shotCooldown == 0) {
			fireHarpoon ();
			shotCooldown += 30;
		}

		if (shotCooldown > 0) {
			shotCooldown -= 1;
		}

	}

	//fire harpoon function
	void fireHarpoon() {
		//create a new harpoon object and apply velocity in the facing direction
		GameObject h = Instantiate (harpoon, new Vector3(cannon.transform.position.x, cannon.transform.position.y, -.9f), cannon.transform.rotation);
		h.GetComponent<Rigidbody2D>().velocity = cannon.transform.right*15;
	}
}