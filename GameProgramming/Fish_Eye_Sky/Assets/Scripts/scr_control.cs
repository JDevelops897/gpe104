﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_control : MonoBehaviour {

	public int score = 0;
	public int time = 0;
	public float minutes = 0;
	public float seconds = 0;
	public int kills = 0;
	public bool gameStarted = false;

	//objects
	public GameObject wave;
	public GameObject Timer;
	public GameObject Score;
	public GameObject Kills;

	public GameObject tutorial;

	public GameObject minnow;
	public GameObject flying;
	public GameObject swordfish;

	private enum states {tutorial, game};
	private states state;

	// Use this for initialization
	void Start () {
		//create 12 waves evenly spaced across the bottom
		for (int i = 0; i < 12; i++) {
			Instantiate (wave, new Vector3 (i*1.4f-8f, -8, -1.25f), Quaternion.identity);
		}
		state = states.tutorial;
	}

	// Update is called once per frame
	void Update () {
		switch (state) {
		case states.tutorial:
			if (Input.anyKey) {
				state = states.game;
				tutorial.gameObject.SetActive (false);
			}
			break;
		case states.game:
			//increment time
			time += 1;
			seconds = Mathf.Floor (time / 60f);
			minutes = Mathf.Floor (seconds / 60f);

			//draw the time to the screen
			if (seconds - minutes * 60 < 10) {
				Timer.GetComponent<Text> ().text = string.Concat (minutes, ":0", (seconds - 60 * minutes));
			} else {
				Timer.GetComponent<Text> ().text = string.Concat (minutes, ":", (seconds - 60 * minutes));
			}
			//draw the score to screen
			Score.GetComponent<Text> ().text = score.ToString ();
			//draw kills to the screen
			Kills.GetComponent<Text> ().text = string.Concat ("Fish Murdered: ", kills);

			//minnow spawn
			if (time % 60 == 0) {
				if (Mathf.Floor (Random.Range (0f, 2f)) == 0) {
					SpawnMinnow (new Vector3 (-5f, -6f, -.5f));
				} else {
					SpawnMinnow (new Vector3 (5f, -6f, -.5f));
				}
			}
			//flying fish
			if (time % 120 == 0) {

				if (Mathf.Floor (Random.Range (0f, 2f)) == 0) {
					SpawnFlyingfish (new Vector3 (-8f, -1f, -.5f));
				} else {
					SpawnFlyingfish (new Vector3 (8f, -1f, -.5f));
				}

				//sword fish
				if (Mathf.Floor (Random.Range (0f, 3f)) == 0) {
					if (Mathf.Floor (Random.Range (0f, 2f)) == 0) {
						SpawnSwordfish (new Vector3 (-4f, -6f, -.5f));
					} else {
						SpawnSwordfish (new Vector3 (4f, -6f, -.5f));
					}
				}
			}
			break;
		}
	}

	void SpawnMinnow(Vector3 v) {
		GameObject m = Instantiate (minnow, v, Quaternion.identity);
		if (v.x < 0) {
			m.GetComponent<Rigidbody2D> ().velocity = (Vector3.right*(Random.Range(2f, 3.5f)) + Vector3.up*(Random.Range(10f, 16f)));
		} else {
			m.GetComponent<Rigidbody2D> ().velocity = (Vector3.left*(Random.Range(2f, 3.5f)) + Vector3.up*(Random.Range(10f, 16f)));
		}
	}

	void SpawnFlyingfish(Vector3 v) {
		GameObject f = Instantiate (flying, v, Quaternion.identity);
		if (v.x < 0) {
			f.GetComponent<Rigidbody2D> ().velocity = (Vector3.right*(Random.Range(8f, 10f)) + Vector3.up*(Random.Range(5f, 8f)));
		} else {
			f.GetComponent<Rigidbody2D> ().velocity = (Vector3.left*(Random.Range(8f, 10f)) + Vector3.up*(Random.Range(5f, 8f)));
		}
	}

	void SpawnSwordfish(Vector3 v) {
		GameObject s = Instantiate (swordfish, v, Quaternion.identity);
		s.GetComponent<Rigidbody2D> ().velocity = (Vector3.up*(Random.Range(10f, 16f)));
	}

}
