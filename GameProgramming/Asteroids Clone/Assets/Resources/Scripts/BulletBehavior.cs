﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour {
	
	public float bulletDecay = 1.0f;
	public int bulletSpeed = 400;

	// Use this for initialization
	void Start () {
		// Set the bullet to destroy itself after 1 seconds
		Destroy (gameObject, bulletDecay);

		// Push the bullet in the direction it is facing
		GetComponent<Rigidbody2D>().AddForce(transform.up * bulletSpeed);
	}

	void OnTriggerExit2D() {
		Destroy(this.gameObject);
	}
}
