﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigAsteroidBehavior : MonoBehaviour {
	public GameObject smallAsteroid;

	private GameObject ship;
	public float asteroidSpeed = 40;
	// Use this for initialization
	void Start () {
		ship = GameObject.Find ("ship(Clone)");
		if (ship != null) {
			GetComponent<Rigidbody2D> ().AddForce ((ship.transform.position - transform.position) * asteroidSpeed);
		} else {
			GetComponent<Rigidbody2D> ().AddForce (new Vector3(Random.Range(-9f, 9f), Random.Range(-9f, 9f), 0) * asteroidSpeed);
		}
		GetComponent<Rigidbody2D> ().angularVelocity = Random.Range (-0.0f, 90f);
	}
	
	// Update is called once per frame
	void Update () {



	}

	void OnTriggerEnter2D(Collider2D c){

		if (c.gameObject.tag == "Bullet") {
			Destroy (c.gameObject);
			Destroy (this.gameObject);
		}
	}

	void OnTriggerExit2D() {
		Destroy(this.gameObject);
	}
}
