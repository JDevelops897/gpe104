﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBehavior : MonoBehaviour {

	public GameObject ship;
	public GameObject BigAsteroid;
	public int spawnTimer = 0;

	public int lives = 3;

	// Use this for initialization
	void Start () {
		Instantiate (ship);
	}
	
	// Update is called once per frame
	void Update () {
		spawnTimer++;
		if (spawnTimer == 60) {
			Instantiate (BigAsteroid, new Vector3(Random.Range(-8f, 8f), Random.Range(-5f, 5f), 0), transform.rotation);
			spawnTimer = 0;
		}
	}
}
