﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehavior : MonoBehaviour {

	public float rotationSpeed = 200.0f;
	public float thrustForce = 3f;

	public GameObject laser;

	//private GameController gameController;

	void Start() {
		
	}

	void Update () {

		// Rotate the ship if necessary
		transform.Rotate(0, 0, System.Convert.ToInt32(Input.GetKey("a")) * rotationSpeed * Time.deltaTime);
		transform.Rotate(0, 0, System.Convert.ToInt32(Input.GetKey("d")) * -rotationSpeed * Time.deltaTime);

		// Thrust the ship if necessary
		GetComponent<Rigidbody2D>().AddForce(transform.up * thrustForce * System.Convert.ToInt32(Input.GetKey("w")));

		// Has a bullet been fired
		if (Input.GetKeyDown("space")) {
			ShootBullet ();
		}
	}

	void OnCollisionEnter2D(Collision2D c){

		// Anything except a bullet is an asteroid
		if (c.gameObject.tag != "Bullet") {
			// Move the ship to the centre of the screen
			transform.position = new Vector3 (0, 0, 0); 

			// Remove all velocity from the ship
			GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, 0, 0);
		}
	}

	void ShootBullet(){
		Instantiate(laser, transform.position+transform.up*.52f, transform.rotation);
	}

	void OnTriggerExit2D(Collider2D c) {
		if (c.gameObject.tag != "Bullet") {
			Destroy (this.gameObject);
		}
	}
}