﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareBehavior : MonoBehaviour {
	//Set variables
	//gravity
	private float gravity = 0.0f;
	//move speed
	private float speed = 0.25f;

	//establish variables for properties
	private Transform tf;
	//private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		//set tf to the current Transform
		tf = GetComponent<Transform>();
		//zero the tf
		tf.position = Vector3.zero;

		//set sr to the current spriterenderer
		//sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		//update gravity 
		if (gravity < 1f && tf.position.y != -4.5f) {
			//increase fall speed if below is empty
			gravity += .01f;
		} else if (gravity > 1f && tf.position.y != -4.5f) {
			//cap fall speed at 1
			gravity = 1f;
		}
		//create temporary movement vector
		Vector3 temp = tf.position;
		//apply gravity
		temp.y -= gravity;
		//if the new position is bottom of the map
		if (temp.y < -4.5f) {
			//keep the y in bounds
			temp.y = -4.5f;
			//reset gravity
			gravity = 0f;
		}

		//inputs
		if (Input.GetKey("right")) {
			temp.x += speed;
		}
		if (Input.GetKey("left")) {
			temp.x -= speed;
		}
		//jump
		if (Input.GetKeyDown("up") && temp.y == -4.5f) {
			gravity = -.25f;
		}

		//apply position changes
		tf.position = temp;

		//sr.color = new Color (Random.value, Random.value, Random.value);
	}
}